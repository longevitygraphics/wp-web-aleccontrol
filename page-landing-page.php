<?php
/**
 * Template Name: Landing Page
 */
get_header(); ?>
<div class="landing-page">

	<?php get_template_part( '/layouts/header' ); ?>

	<div id="home" class="banner" style="background-image: url(<?php echo get_field( 'banner_bg_image' )['url'] ?>)">
		<div class="container">
			<div class="banner__content">
				<?php echo get_field( "banner_content" ) ?>
				<a class="btn btn-primary"
				   href="<?php echo get_field( 'banner_link' )['url'] ?>"><?php echo get_field( 'banner_link' )['title'] ?></a>
			</div>
		</div>
	</div>

	<div class="landing-page-content">
		<?php
		while ( have_rows( 'lg_layouts' ) ): the_row();

			switch ( get_row_layout() ) {
				case "features":
					get_template_part( '/layouts/features' );
					break;
				case "image_and_text":
					get_template_part( '/layouts/image_and_text' );
					break;
				case "text":
					get_template_part( '/layouts/text' );
					break;
				case "testimonials":
					get_template_part( '/layouts/testimonials' );
					break;
				case "newsletter_form":
					get_template_part( '/layouts/newsletter_form' );
					break;
			}

		endwhile;
		?>
	</div>
</div>

<?php get_footer(); ?>
