<div id="testimonials" class="lg-carousel">
	<div class="container">
		<h2><?php echo get_sub_field( "testimonial_title" ) ?>
			<i class="fa fa-quote-left" aria-hidden="true"></i>
		</h2>

		<div id="testimonialCarousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<?php
				$slider_counter = - 1;
				while ( have_rows( "testimonial_list" ) ): the_row();
					$slider_counter ++;
					?>
					<li
						data-target="#testimonialCarousel"
						data-slide-to="<?php echo $slider_counter ?>"
						class="<?php echo $slider_counter === 0 ? 'active' : ''; ?>">
					</li>
				<?php endwhile; ?>
			</ol>
			<div class="carousel-inner">

				<?php
				$slider_counter = - 1;
				while ( have_rows( "testimonial_list" ) ): the_row();
					$slider_counter ++;
					?>

					<div class="carousel-item <?php echo $slider_counter === 0 ? 'active' : ''; ?>">
						<div class="carousel-caption">
							<?php echo get_sub_field( "content" ) ?>
							<h5>
								<?php echo get_sub_field( 'author' ) ?>
								<span><?php echo get_sub_field( 'designation' ) ?></span>
							</h5>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
			<!--<a class="carousel-control-prev" href="#testimonialCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#testimonialCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>-->
		</div>
	</div>

</div>
