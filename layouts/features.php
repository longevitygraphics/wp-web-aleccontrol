<div id="features" class="features-wrap">
	<div class="container">
		<div class="features">
			<div class="features__copy"><?php echo get_sub_field( 'features_copy' ) ?></div>
			<div class="features__list">
				<?php while ( have_rows( 'feature_list' ) ): the_row(); ?>
					<div class="features__item">
						<div class="features__icon">
							<?php
							$icon_image = get_sub_field("icon");
							?>
							<img src="<?php echo $icon_image['url'] ?>" alt="<?php echo $icon_image['alt'] ?>">
						</div>
						<div class="features__content">
							<h3><?php echo get_sub_field( 'title' ) ?></h3>
							<?php echo get_sub_field( 'para' ) ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</div>

