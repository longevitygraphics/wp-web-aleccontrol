<!-- TODO ???
<script type="text/javascript" src='http://aleccontrol.com/SuiteCRM/cache/include/javascript/sugar_grp1.js?v=3RaCpB-SMnlHAeYipbpyLg'></script>
-->

<div id="contact" class="newsletter-form">


	<div class="container">

		<div class="newsletter-form__inner">
			<form id="WebToLeadForm" action="http://aleccontrol.com/SuiteCRM/index.php?entryPoint=WebToPersonCapture"
			      method="POST" name="WebToLeadForm">
				<div class="newsletter-form__copy">
					<?php echo get_sub_field("newsletter_form-content")  ?>
				</div>
				<div class="form-group">
					<label class="sr-only" for="first_name">First Name*</label>
					<input id="first_name" name="first_name" required="" type="text" class="form-control"
					       placeholder="First Name*"/>
				</div>
				<div class="form-group">
					<label class="sr-only" for="last_name">Last Name*</label>
					<input id="last_name" name="last_name" required="" type="text" class="form-control"
					       placeholder="Last Name*"/>
				</div>
				<div class="form-group">
					<label class="sr-only" for="email1">Email*</label>
					<input id="email1" name="email1" required="" type="email" class="form-control"
					       placeholder="Email*"/>
				</div>

				<div class="form-group">
					<label class="sr-only" for="phone_work">Phone Number</label>
					<input id="phone_work" name="phone_work" type="text" class="form-control"
					       placeholder="Phone Number"/>
				</div>
				<div class="form-group">
					<label class="sr-only" for="primary_address_country">Country*</label>
					<input id="primary_address_country" name="primary_address_country" required="" type="text"
					       class="form-control" placeholder="Country*"/>
				</div>
				<div class="form-group">
					<label class="sr-only" for="primary_address_state">State/Province*</label>
					<input id="primary_address_state" name="primary_address_state" required="" type="text"
					       class="form-control" placeholder="State/Province*"/>
				</div>
				<div class="form-group">
					<label class="sr-only" for="primary_address_city">City*</label>
					<input id="primary_address_city" name="primary_address_city" required="" type="text"
					       class="form-control" placeholder="City*"/>
				</div>
				<div class="form-group">
					<label class="sr-only" for="description">Your Lighting Objective/Question* (Please include short
						description of your current space and lighting type)</label>
					<textarea id="description"
					          class="form-control"
					          cols="80"
					          name="description"
					          rows="4"
					          required
					          placeholder="Your Lighting Objective/Question* &#10;(Please include short description of your current space and lighting type)"></textarea>
				</div>

				<button class="btn btn-primary" onclick="submit_form();">Submit</button>


				<input id="campaign_id" name="campaign_id" type="hidden"
				       value="c7bf52ed-6d89-3087-427f-5c75d22f82b3"/>
				<input id="redirect_url"
				       name="redirect_url" type="hidden"
				       value="http://aleccontrol.com/thank_you"/>
				<input id="assigned_user_id"
				       name="assigned_user_id" type="hidden"
				       value="1"/>
				<input id="moduleDir"
				       name="moduleDir" type="hidden"
				       value="Leads"/>

			</form>
		</div>
	</div>
</div>


<script type="text/javascript">// <![CDATA[
  function typeSelectfunction() {
    var e = document.getElementById("typeSelect");
    var t = document.getElementById("customer_type_c");
    var typeSelected = e.options[e.selectedIndex].value;
    if (typeSelected == 'Others') {
      document.getElementById("custType").style.display = 'block';
      t.value = "";
      t.focus();
    } else {
      t.value = typeSelected;
      document.getElementById("custType").style.display = 'none';
    }
  }

  function submit_form() {
    debugger
    // document.getElementById("custType").style.display= 'block';
    if (typeof (validateCaptchaAndSubmit) != 'undefined') {
      validateCaptchaAndSubmit();
    } else {
      check_webtolead_fields();
      //document.WebToLeadForm.submit();
    }
  }

  function check_webtolead_fields() {
    if (document.getElementById('bool_id') != null) {
      var reqs = document.getElementById('bool_id').value;
      bools = reqs.substring(0, reqs.lastIndexOf(';'));
      var bool_fields = new Array();
      var bool_fields = bools.split(';');
      nbr_fields = bool_fields.length;
      for (var i = 0; i < nbr_fields; i++) {
        if (document.getElementById(bool_fields[i]).value == 'on') {
          document.getElementById(bool_fields[i]).value = 1;
        } else {
          document.getElementById(bool_fields[i]).value = 0;
        }
      }
    }
  } // ]]>
</script>
