<div id="<?php echo get_sub_field( 'wrapper_id' ) ?>" class="<?php echo get_sub_field( 'wrapper_class' ) ?>">
	<div class="container">
		<div class="img-text row">
			<div class="col-md-6">
				<img src="<?php echo get_sub_field( 'image' )['url'] ?>"
				     alt="<?php echo get_sub_field( 'image' )['alt'] ?>">
			</div>
			<div class="col-md-6 img-text__content">
				<?php echo get_sub_field( 'content' ) ?>
			</div>
		</div>
	</div>
</div>

