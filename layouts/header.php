<nav class="navbar navbar-expand-lg navbar-light fixed-top bg-light">
	<div class="container">
	<a class="navbar-brand" href="#">
		<img width="130"
		     src="https://aleccontrol.com/wordpress/wp-content/uploads/2018/02/cropped-AlecLogo-1-2.jpg"
		     class="custom-logo" alt="" itemprop="logo"></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
	        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav ml-auto">
			<li class="nav-item active">
				<a class="nav-link" href="#home">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#features">Features</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#benefits">Benefits</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#product">Product</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#testimonials">Testimonials</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#contact">Contact</a>
			</li>
			<li class="nav-item">
				<a class="nav-link btn btn-primary" href="#contact">Get Pricing</a>
			</li>

		</ul>
	</div>
	</div>
</nav>
