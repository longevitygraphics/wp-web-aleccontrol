<?php
function lg_enqueue_styles() {

	$parent_style = 'twentysixteen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get( 'Version' )
	);
	if ( get_page_template_slug() == 'page-landing-page.php' ) {
		wp_enqueue_style( 'child-style-main',
			get_stylesheet_directory_uri() . '/assets/dist/css/main.css',
			array( $parent_style ),
			wp_get_theme()->get( 'Version' )
		);

		wp_enqueue_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', null, null, true );
	}


}

add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles' );


add_action( 'widgets_init', 'lg_theme_widgets_init' );
function lg_theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Landing Page Footer Area 1', 'lg' ),
		'id'            => 'lg-footer-area-1',
		'description'   => __( 'Footer Area 1', 'lg' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Landing Page Footer Area 2', 'lg' ),
		'id'            => 'lg-footer-area-2',
		'description'   => __( 'Footer Area 2', 'lg' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Landing Page Footer Area 3', 'lg' ),
		'id'            => 'lg-footer-area-3',
		'description'   => __( 'Footer Area 3', 'lg' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	) );
}


function pr( $a ) {
	echo '<pre>';
	print_r( $a );
	echo '</pre>';
}
